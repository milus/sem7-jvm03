import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Wojciech Milewski
 */
public class InterpreterTest {

    @Test
    public void shouldInterpreterWork() throws Exception {
        FileInputStream fileInputStream = new FileInputStream(getFilePath("exception/n1.txt"));

        new Interpreter(fileInputStream, 5000).interpret();



    }

    private static String getFilePath(String filename) {
        return "src/test/resources/" + filename;
    }
}
