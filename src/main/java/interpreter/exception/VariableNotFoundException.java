package interpreter.exception;

/**
 * @author Wojciech Milewski
 */
public class VariableNotFoundException extends RuntimeException {
    public VariableNotFoundException(String name) {
        super("Variable not found: " + name);
    }

    public VariableNotFoundException() {
        super();
    }
}
