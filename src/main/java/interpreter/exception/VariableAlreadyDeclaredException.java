package interpreter.exception;

/**
 * @author Wojciech Milewski
 */
public class VariableAlreadyDeclaredException extends RuntimeException {
    public VariableAlreadyDeclaredException(String name) {
        super("Variable has been already declared: " + name);
    }
}
