package interpreter;

/**
 * @author Wojciech Milewski
 */
public class ObjectInfo {
    private ObjectType type;
    private int pointer;

    public ObjectInfo(ObjectType type) {
        this(type, 0);
        if (type != ObjectType.S_NULL) {
            throw new IllegalArgumentException("Used only for null");
        }
    }

    public ObjectInfo(ObjectType type, int pointer) {
        this.type = type;
        this.pointer = pointer;
    }

    public ObjectType getType() {
        return type;
    }

    public int getPointer() {
        if (type == ObjectType.S_NULL) {
            throw new NullPointerException("You can't call getPointer() method for S_NULL type");
        }
        return pointer;
    }

    public void setType(ObjectType type) {
        this.type = type;
    }

    public void setPointer(int pointer) {
        this.pointer = pointer;
    }
}
