package interpreter;

/**
 * @author Wojciech Milewski
 */
public class VarT {
    private int pointer;
    private int f1Pointer;
    private int f2Pointer;
    private int data;

    public VarT(int pointer, int f1Pointer, int f2Pointer, int data) {
        this.pointer = pointer;
        this.f1Pointer = f1Pointer;
        this.f2Pointer = f2Pointer;
        this.data = data;
    }

    public int getPointer() {
        return pointer;
    }

    public void setPointer(int pointer) {
        this.pointer = pointer;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public int getF1Pointer() {
        return f1Pointer;
    }

    public void setF1Pointer(int f1Pointer) {
        this.f1Pointer = f1Pointer;
    }

    public int getF2Pointer() {
        return f2Pointer;
    }

    public void setF2Pointer(int f2Pointer) {
        this.f2Pointer = f2Pointer;
    }
}
