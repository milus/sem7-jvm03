package interpreter;

/**
 * @author Wojciech Milewski
 */
public enum LValType {
    S_VARIABLE, T_VARIABLE, T_F1_FIELD, T_F2_FIELD, T_DATA_FIELD
}
