package interpreter;

/**
 * @author Wojciech Milewski
 */
public enum ObjectType {
    S_NULL, S, T
}
