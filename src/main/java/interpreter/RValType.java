package interpreter;

/**
 * @author Wojciech Milewski
 */
public enum RValType {
    S_VARIABLE, T_VARIABLE, NULL, INT, STRING
}
