package antlr;// Generated from NPJ.g4 by ANTLR 4.5

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link NPJParser}.
 */
public interface NPJListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link NPJParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(NPJParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(NPJParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterStatements(NPJParser.StatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitStatements(NPJParser.StatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(NPJParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(NPJParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#varDecl}.
	 * @param ctx the parse tree
	 */
	void enterVarDecl(NPJParser.VarDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#varDecl}.
	 * @param ctx the parse tree
	 */
	void exitVarDecl(NPJParser.VarDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#varDeclT}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclT(NPJParser.VarDeclTContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#varDeclT}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclT(NPJParser.VarDeclTContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#varDeclSWithString}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclSWithString(NPJParser.VarDeclSWithStringContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#varDeclSWithString}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclSWithString(NPJParser.VarDeclSWithStringContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#varDeclSWithNull}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclSWithNull(NPJParser.VarDeclSWithNullContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#varDeclSWithNull}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclSWithNull(NPJParser.VarDeclSWithNullContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(NPJParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(NPJParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#rvalue}.
	 * @param ctx the parse tree
	 */
	void enterRvalue(NPJParser.RvalueContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#rvalue}.
	 * @param ctx the parse tree
	 */
	void exitRvalue(NPJParser.RvalueContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#lvalue}.
	 * @param ctx the parse tree
	 */
	void enterLvalue(NPJParser.LvalueContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#lvalue}.
	 * @param ctx the parse tree
	 */
	void exitLvalue(NPJParser.LvalueContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#printStringInQuotes}.
	 * @param ctx the parse tree
	 */
	void enterPrintStringInQuotes(NPJParser.PrintStringInQuotesContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#printStringInQuotes}.
	 * @param ctx the parse tree
	 */
	void exitPrintStringInQuotes(NPJParser.PrintStringInQuotesContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#printVarDeclS}.
	 * @param ctx the parse tree
	 */
	void enterPrintVarDeclS(NPJParser.PrintVarDeclSContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#printVarDeclS}.
	 * @param ctx the parse tree
	 */
	void exitPrintVarDeclS(NPJParser.PrintVarDeclSContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#heapAnalyze}.
	 * @param ctx the parse tree
	 */
	void enterHeapAnalyze(NPJParser.HeapAnalyzeContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#heapAnalyze}.
	 * @param ctx the parse tree
	 */
	void exitHeapAnalyze(NPJParser.HeapAnalyzeContext ctx);
	/**
	 * Enter a parse tree produced by {@link NPJParser#collect}.
	 * @param ctx the parse tree
	 */
	void enterCollect(NPJParser.CollectContext ctx);
	/**
	 * Exit a parse tree produced by {@link NPJParser#collect}.
	 * @param ctx the parse tree
	 */
	void exitCollect(NPJParser.CollectContext ctx);
}