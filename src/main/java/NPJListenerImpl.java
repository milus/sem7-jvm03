import antlr.NPJBaseListener;
import antlr.NPJParser;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import interpreter.*;
import interpreter.exception.VariableAlreadyDeclaredException;
import interpreter.exception.VariableNotFoundException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Wojciech Milewski
 */
public class NPJListenerImpl extends NPJBaseListener {

    private final HeapImpl heap;
    private final Map<String, ObjectInfo> variables;

    public NPJListenerImpl(int heapSize) {
        heap = new HeapImpl(heapSize);
        variables = new HashMap<String, ObjectInfo>();
    }

    @Override
    public void exitPrintStringInQuotes(NPJParser.PrintStringInQuotesContext ctx) {
        if (ctx.STRING() == null) {
            NPJ.print("NULL");
        } else {
            NPJ.print(ctx.STRING().getText());
        }
    }

    @Override
    public void exitPrintVarDeclS(NPJParser.PrintVarDeclSContext ctx) {
        ObjectInfo objectInfo = get(ctx.STRING().getText());

        if (objectInfo.getType() == ObjectType.S_NULL) {
            NPJ.print("NULL");
        } else if (objectInfo.getType() == ObjectType.S) {
            String value = heap.getS(objectInfo.getPointer());
            NPJ.print(value);
        }
    }

    @Override
    public void exitVarDeclSWithNull(NPJParser.VarDeclSWithNullContext ctx) {
        String name = ctx.STRING().getText();
        ObjectInfo info = new ObjectInfo(ObjectType.S_NULL);
        put(name, info);
    }

    @Override
    public void exitVarDeclSWithString(NPJParser.VarDeclSWithStringContext ctx) {
        String name = ctx.STRING(0).getText();
        String value = ctx.STRING(1).getText();

        int pointer = heap.allocateS(value, variables);
        ObjectInfo info = new ObjectInfo(ObjectType.S, pointer);
        put(name, info);
    }

    @Override
    public void exitVarDeclT(NPJParser.VarDeclTContext ctx) {
        String name = ctx.STRING().getText();
        int pointer = heap.allocateT(variables);
        ObjectInfo info = new ObjectInfo(ObjectType.T, pointer);
        put(name, info);
    }

    @Override
    public void exitAssignment(NPJParser.AssignmentContext ctx) {
        String lval = ctx.lvalue().getText();
        String rval = ctx.rvalue().getText();

        LValType lValType = resolveLvalType(lval);
        RValType rValType = resolveRvalType(rval);

        switch (lValType) {
            case S_VARIABLE:
                switch (rValType) {
                    case NULL:
                        get(getVariableName(lval)).setType(ObjectType.S_NULL);
                        break;
                    case STRING:
                        ObjectInfo info = get(getVariableName(lval));
                        info.setType(ObjectType.S_NULL);        //to make it collectible by GC
                        int newPointer = heap.allocateS(rval.substring(1, rval.length() - 1), variables);
                        info.setPointer(newPointer);
                        info.setType(ObjectType.S);
                        break;
                    case S_VARIABLE:
                        ObjectInfo lvalInfo = get(getVariableName(lval));
                        ObjectInfo rvalInfo = get(getVariableName(rval));
                        ObjectType rvalType = rvalInfo.getType();
                        if (rvalType != ObjectType.S_NULL) {
                            lvalInfo.setPointer(rvalInfo.getPointer());
                        }
                        lvalInfo.setType(rvalInfo.getType());
                        break;
                    default:
                        throw new RuntimeException("Cannot assign such rValue type to VarS");
                }
                break;
            case T_VARIABLE:
                switch (rValType) {
                    case T_VARIABLE: {
                        ObjectInfo lvalInfo = get(getVariableName(lval));
                        String rvalName = getVariableName(rval);
                        ObjectInfo rvalInfo = get(rvalName);

                        VarT rValT = heap.getT(rvalInfo.getPointer(), rvalName, rval);
                        lvalInfo.setPointer(rValT.getPointer());
                        break;
                    }
                    case NULL: {
                        ObjectInfo lvalInfo = get(getVariableName(lval));
                        lvalInfo.setPointer(HeapImpl.NULL_POINTER);
                        break;
                    }
                    default:
                        throw new RuntimeException(String.format("Cannot assign rvalue %s to VarT", rValType));
                }
                break;
            case T_F1_FIELD:
                switch (rValType) {
                    case T_VARIABLE: {
                        String lvalName = getVariableName(lval);
                        String rvalName = getVariableName(rval);

                        ObjectInfo lvalInfo = get(lvalName);
                        ObjectInfo rvalInfo = get(rvalName);

                        VarT lValT = heap.getT(lvalInfo.getPointer(), lvalName, lval.substring(0, lval.length() - 3));
                        VarT rValT = heap.getT(rvalInfo.getPointer(), rvalName, rval);
                        lValT.setF1Pointer(rValT.getPointer());
                        heap.updateT(lValT);
                        break;
                    }
                    case NULL: {
                        String lvalName = getVariableName(lval);
                        ObjectInfo lvalInfo = get(lvalName);

                        VarT lValT = heap.getT(lvalInfo.getPointer(), lvalName, lval.substring(0, lval.length() - 3));
                        lValT.setF1Pointer(HeapImpl.NULL_POINTER);
                        heap.updateT(lValT);
                        break;
                    }
                    default:
                        throw new RuntimeException(String.format("Cannot assign rvalue %s to VarT", rValType));
                }
                break;
            case T_F2_FIELD:
                switch (rValType) {
                    case T_VARIABLE: {
                        String lvalName = getVariableName(lval);
                        String rvalName = getVariableName(rval);

                        ObjectInfo lvalInfo = get(lvalName);
                        ObjectInfo rvalInfo = get(rvalName);

                        VarT lValT = heap.getT(lvalInfo.getPointer(), lvalName, lval.substring(0, lval.length() - 3));
                        VarT rValT = heap.getT(rvalInfo.getPointer(), rvalName, rval);
                        lValT.setF2Pointer(rValT.getPointer());
                        heap.updateT(lValT);
                        break;
                    }
                    case NULL: {
                        String lvalName = getVariableName(lval);
                        ObjectInfo lvalInfo = get(lvalName);

                        VarT lValT = heap.getT(lvalInfo.getPointer(), lvalName, lval.substring(0, lval.length() - 3));
                        lValT.setF2Pointer(HeapImpl.NULL_POINTER);
                        heap.updateT(lValT);
                        break;
                    }
                    default:
                        throw new RuntimeException(String.format("Cannot assign rvalue %s to VarT", rValType));
                }
                break;
            case T_DATA_FIELD:
                switch (rValType) {
                    case INT:
                        int newData;
                        try {
                            newData = Integer.parseInt(rval);
                        } catch (NumberFormatException e) {
                            String rvalName = getVariableName(rval);
                            ObjectInfo rvalInfo = get(rvalName);
                            VarT rValT = heap.getT(rvalInfo.getPointer(), rvalName, rval);
                            newData = rValT.getData();
                        }
                        String lvalName = getVariableName(lval);
                        ObjectInfo lvalInfo = get(lvalName);
                        VarT lValT = heap.getT(lvalInfo.getPointer(), lvalName, lval);
                        lValT.setData(newData);
                        heap.updateT(lValT);
                        break;
                    default:
                        throw new RuntimeException(String.format("Cannot assign rvalue %s to VarT", rValType));
                }
                break;
        }
    }

    @Override
    public void exitHeapAnalyze(NPJParser.HeapAnalyzeContext ctx) {
        heap.refreshStats();
        List<Integer> integers = heap.getAllIntegers();
        List<String> strings = heap.getAllStrings();

        NPJ.heapAnalyze(integers, strings);
    }

    @Override
    public void exitCollect(NPJParser.CollectContext ctx) {
        heap.collect(heap.getHeap(), (Map) variables);
    }

    private String getVariableName(String val) {
        //todo implement without Guava
        return Lists.newArrayList(Splitter.on('.').limit(2).split(val)).get(0);
    }

    private LValType resolveLvalType(String lval) {
        if (!lval.contains(".")) {
            if (variables.containsKey(lval)) {
                ObjectType type = get(lval).getType();
                switch (type) {
                    case S:
                    case S_NULL:
                        return LValType.S_VARIABLE;
                    case T:
                        return LValType.T_VARIABLE;
                    default:
                        throw new RuntimeException("Unknown type of variable");
                }
            } else {
                throw new RuntimeException(String.format("Cannot assign to variable %s, because it wasn't declared", lval));
            }
        } else {
            List<String> strings = Splitter.on('.').splitToList(lval);

            // first string should be variable name
            if (strings.size() < 1 || !variables.containsKey(strings.get(0))) {
                throw new RuntimeException("No such declared variable");
            }

            //check dereferenced values
            for (int i = 1; i < strings.size() - 1; i++) {
                if (!strings.get(i).equals("f1") && !strings.get(i).equals("f2")) {
                    throw new RuntimeException("Structure does not contain " + strings.get(i) + " field of type T");
                }
            }

            String dereferencedValue = strings.get(strings.size() - 1);
            if (dereferencedValue.equals("f1")) {
                return LValType.T_F1_FIELD;
            } else if (dereferencedValue.equals("f2")) {
                return LValType.T_F2_FIELD;
            } else if (dereferencedValue.equals("data")) {
                return LValType.T_DATA_FIELD;
            } else {
                throw new RuntimeException("Structure does not contain " + dereferencedValue + " field");
            }
        }
    }

    private RValType resolveRvalType(String rval) {
        if (rval.equals("NULL")) {
            return RValType.NULL;
        }

        if (rval.startsWith("\"") && rval.endsWith("\"")) {
            return RValType.STRING;
        }

        try {
            Integer.parseInt(rval);
            return RValType.INT;
        } catch (NumberFormatException e) {
            //not an int so try to evaluate as lval
        }

        LValType lValType = resolveLvalType(rval);
        switch (lValType) {
            case S_VARIABLE:
                return RValType.S_VARIABLE;
            case T_VARIABLE:
            case T_F1_FIELD:
            case T_F2_FIELD:
                return RValType.T_VARIABLE;
            case T_DATA_FIELD:
                return RValType.INT;
            default:
                throw new RuntimeException("Unknown type of RValue");
        }

    }


    @Override
    public void exitProgram(NPJParser.ProgramContext ctx) {

        super.exitProgram(ctx);
    }

    private void put(String name, ObjectInfo info) {
        if (variables.containsKey(name)) {
            throw new VariableAlreadyDeclaredException(name);
        }
        variables.put(name, info);
    }

    private ObjectInfo get(String name) {
        if (!variables.containsKey(name)) {
            throw new VariableNotFoundException(name);
        }
        return variables.get(name);
    }
}
