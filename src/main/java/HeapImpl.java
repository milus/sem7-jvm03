import interpreter.ObjectInfo;
import interpreter.ObjectType;
import interpreter.VarT;
import interpreter.exception.HeapOverflowException;

import java.util.*;

/**
 * @author Wojciech Milewski
 */
public class HeapImpl implements Collector {
    public static final int NULL_POINTER = 0;

    public static final int S_TYPE = -1;
    public static final int T_TYPE = -2;

    private final int firstPartStart;
    private final int secondPartStart;

    private final int[] heap;

    private int firstFreePointer;
    private int maxPointer;
    private boolean useFirstPart = true;

    private List<Integer> intStats = Collections.emptyList();
    private List<String> stringStats = Collections.emptyList();

    public HeapImpl(int maxSize) {

        this.firstPartStart = 1;
        this.secondPartStart = maxSize / 2 + 1;

        this.firstFreePointer = firstPartStart;
        this.maxPointer = maxSize;

        this.heap = new int[maxSize];
    }

    // CAPACITY CHECKING

    private void ensureCapacityForS(String value, Map<String, ObjectInfo> variables) {
        ensureCapacity(2 + value.length(), variables);
    }

    private void ensureCapacityForT(Map<String, ObjectInfo> variables) {
        ensureCapacity(2, variables);
    }

    private void ensureCapacity(int addedSize, Map<String, ObjectInfo> variables) {
        ensureCapacity(firstFreePointer, addedSize, variables);
    }

    private void ensureCapacity(int pointer, int addedSize, Map<String, ObjectInfo> variables) {
        if (pointer + addedSize > maxPointer) {
            //todo call collector
            collect(heap, (Map) variables);
            throw new HeapOverflowException();
        }
    }

    // WRITE METHODS

    public final int allocateS(String value, Map<String, ObjectInfo> variables) {
        ensureCapacityForS(value, variables);
        int pointer = this.firstFreePointer;

        heap[this.firstFreePointer++] = S_TYPE;
        heap[this.firstFreePointer++] = value.length();
        for (char c : value.toCharArray()) {
            heap[this.firstFreePointer++] = (int) c;
        }
        return pointer;
    }

    public final int allocateT(Map<String, ObjectInfo> variables) {
        ensureCapacityForT(variables);

        int pointer = this.firstFreePointer;
        heap[this.firstFreePointer++] = T_TYPE;
        heap[this.firstFreePointer++] = NULL_POINTER;
        heap[this.firstFreePointer++] = NULL_POINTER;
        heap[this.firstFreePointer++] = 0;

        return pointer;
    }

    // READ METHODS

    public final String getS(int pointer) {
        if (pointer == NULL_POINTER) {
            throw new NullPointerException();
        }

        int startIndex = pointer + 2;

        int type = heap[pointer];
        if (type != S_TYPE) {
            throw new IllegalArgumentException("Incorrect variable type");
        }

        int length = heap[pointer + 1];
        int endIndex = startIndex + length;

        StringBuilder sb = new StringBuilder(length);
        for (int i = startIndex; i < endIndex; i++) {
            int intValue = heap[i];
            sb.append(((char) intValue));
        }
        return sb.toString();
    }

    public final void refreshStats() {
        intStats = new LinkedList<Integer>();
        stringStats = new LinkedList<String>();

        int index = useFirstPart ? firstPartStart : secondPartStart;

        while (index < maxPointer) {
            if (heap[index] == S_TYPE) {
                String s = getS(index);
                stringStats.add(s);
                index += (2 + s.length());
            } else if (heap[index] == T_TYPE) {
                intStats.add(heap[index + 3]);
                index += 4;
            } else {
                index++;
            }
        }
    }

    public final List<Integer> getAllIntegers() {
        return intStats;
    }

    public final List<String> getAllStrings() {
        return stringStats;
    }

    /**
     * Returns VarT instance corresponding to dereferenced value.
     * <p/>
     * If variableName == path and (pointer == NULL),  then returns VarT with nulls
     * If variableName == path and (pointer -> VarT),  then returns VarT
     * If variableName == path and (pointer -> !VarT), then throws RuntimeException
     * <p/>
     * If variableName != path and (pointer == NULL),  then throws NullPointerException
     * If variableName != path and (pointer -> VarT),  then returns VarT
     * If variableName != path and (pointer -> !VarT), then throws RuntimeException
     *
     * @param pointer      pointing to 'variable'
     * @param variableName 'variable'
     * @param path         variable.f1.f2
     */
    public final VarT getT(int pointer, String variableName, String path) {
        if (variableName.equals(path)) {
            if (pointer == NULL_POINTER) {
                return getNullVarT();
            } else if (heap[pointer] == T_TYPE) {
                return getVarTOn(pointer);
            } else {
                throw new RuntimeException("Pointer does not points to VarT");
            }
        }

        if (pointer == NULL_POINTER) {
            throw new NullPointerException(String.format("%s (variableName) != %s (dereference path), but pointer points to NULL", variableName, path));
        }

        ensureTypeT(heap[pointer]);

        if (!path.startsWith(variableName + ".")) {
            throw new RuntimeException(String.format("Incorrect path: %s", path));
        }

        return getT(pointer, path.substring(variableName.length() + 1));
    }

    private void ensureTypeT(int pointer) {
        if (pointer != T_TYPE) {
            throw new RuntimeException("Pointer does not points to VarT");
        }
    }

    /**
     * Decides how to dereference pointer.
     * <p/>
     * Throws NullPointerException when pointer == NULL_POINTER.
     */
    private VarT getT(int pointer, String path) {
        if (pointer == NULL_POINTER) {
            throw new NullPointerException(String.format("Cannot move to %s, as dereferenced value is null", path));
        }

        ensureTypeT(heap[pointer]);

        if (path.equals("f1")) {
            return getVarTOn(heap[pointer + 1]);
        } else if (path.equals("f2")) {
            return getVarTOn(heap[pointer + 2]);
        } else if (path.equals("data")) {
            return getVarTOn(pointer);
        }

        if (path.startsWith("f1.")) {
            return getT(heap[pointer + 1], path.substring(3));
        } else if (path.startsWith("f2.")) {
            return getT(heap[pointer + 2], path.substring(3));
        } else {
            throw new RuntimeException(String.format("Incorrect path: %s", path));
        }
    }

    private VarT getNullVarT() {
        return new VarT(NULL_POINTER, NULL_POINTER, NULL_POINTER, 0);
    }

    private VarT getVarTOn(int pointer) {
        if (pointer == NULL_POINTER) {
            return getNullVarT();
        }

        ensureTypeT(heap[pointer]);
        return new VarT(pointer, heap[pointer + 1], heap[pointer + 2], heap[pointer + 3]);
    }

    public final void updateT(VarT varT) {
        int pointer = varT.getPointer();
        ensureTypeT(heap[pointer]);

        heap[pointer + 1] = varT.getF1Pointer();
        heap[pointer + 2] = varT.getF2Pointer();
        heap[pointer + 3] = varT.getData();
    }

    public final int[] getHeap() {
        return heap;
    }

    @Override
    public void collect(int[] heap, Map<Object, Object> params) {
        if (this.heap != heap) {
            throw new RuntimeException("Hey! You have passed not my heap!");
        }

        swapHalvesOfWrite();

        @SuppressWarnings("unchecked") Map<String, ObjectInfo> variables = (Map) params;

        // points only to varT
        Map<Integer, Integer> pointerMapping = new HashMap<Integer, Integer>(params.size());

        // move all object to new half
        for (ObjectInfo info : variables.values()) {
            if (info.getType() == ObjectType.S) {
                int newPointer = allocateS(getS(info.getPointer()), variables);
                info.setPointer(newPointer);
            } else if (info.getType() == ObjectType.T) {
                info.setPointer(reallocateT(pointerMapping, info.getPointer(), variables));
            }
        }
        // previous half cleanup
        if (useFirstPart) {
            Arrays.fill(heap, secondPartStart, heap.length, 0);
        } else {
            Arrays.fill(heap, firstPartStart, secondPartStart, 0);
        }
    }

    private int reallocateT(Map<Integer, Integer> pointerMapping, int pointer, Map<String, ObjectInfo> variables) {
        if (pointer == NULL_POINTER) {
            return NULL_POINTER;
        }

        if (pointerMapping.containsKey(pointer)) {
            return pointerMapping.get(pointer);
        }

        VarT varT = getVarTOn(pointer);
        int newPointer = allocateT(variables);
        pointerMapping.put(pointer, newPointer);

        varT.setPointer(newPointer);
        varT.setF1Pointer(reallocateT(pointerMapping, varT.getF1Pointer(), variables));
        varT.setF2Pointer(reallocateT(pointerMapping, varT.getF2Pointer(), variables));

        updateT(varT);

        return newPointer;
    }

    private void swapHalvesOfWrite() {
        if (useFirstPart) {
            firstFreePointer = secondPartStart;
            maxPointer = heap.length;
        } else {
            firstFreePointer = firstPartStart;
            maxPointer = secondPartStart;
        }
        useFirstPart = !useFirstPart;
    }
}
