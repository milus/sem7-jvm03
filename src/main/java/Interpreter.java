import antlr.NPJLexer;
import antlr.NPJParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Interpreter {

    private static final String NPJ_HEAP_SIZE = "npj.heap.size";

    private final InputStream programStream;
    private final int heapSize;

    /**
     * Initializes Njp interpreter with specified program and heap size.
     * 
     * @param programFile
     *            path to program file
     * @param heapSize
     *            heap size
     */
    public Interpreter(String programFile, int heapSize) {
        // get program code stream
        programStream = getProgramStream(programFile);
        this.heapSize = heapSize;
    }

    public Interpreter(InputStream programStream, int heapSize) {
        this.programStream = programStream;
        this.heapSize = heapSize;
    }

    /**
     * Opens program file and returns program code input stream.
     * 
     * @param programFile
     *            path to program file
     * @return input stream for program code
     */
    private InputStream getProgramStream(String programFile) {
        InputStream programStream = null;
        try {
            programStream = new FileInputStream(programFile);
        } catch (FileNotFoundException e) {
            System.out.println("Could not open program file: " + programFile + ", exiting.");
            System.exit(2);
        }

        return programStream;
    }

    public void interpret() throws IOException {
        try {
            NPJLexer lexer = new NPJLexer(new ANTLRInputStream(programStream));
            TokenStream tokens = new CommonTokenStream(lexer);
            NPJParser parser = new NPJParser(tokens);
            parser.addParseListener(new NPJListenerImpl(heapSize));
            parser.program();
        } finally {
            programStream.close();
        }
    }

	private static int getHeapSize() {
		return Integer.valueOf(System.getProperty(NPJ_HEAP_SIZE));
	}


    public static void main(String[] args) {
        // make sure we got one and only one parameter
        if (args.length != 1) {
            System.out.println("Wrong number of arguments - pass program file name");
            System.exit(1);
        }

        // get heap size
        int heapSize = getHeapSize();

        // initialize Njp VM with program file name, heap size and GC
        Interpreter interpreter = new Interpreter(args[0], heapSize);
        try {
            interpreter.interpret();
        } catch (IOException e) {
            System.out.println("Interpretation error: " + e.getMessage());
            System.exit(1);
        }
    }
}
